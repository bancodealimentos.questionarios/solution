using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe PerguntaConfiguracaoDomain - Herda características de BaseDomain
    /// </summary>
    public class PerguntaConfiguracaoDomain:BaseDomain
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Campo { get; set; }

        [Required]
        [StringLength(20)]
        public string Valor { get; set; }

        [Required]
        [StringLength(50)]
        public string TipoValor { get; set; }

        [ForeignKey("IdPergunta")]
        public PerguntaDomain Pergunta { get; set; }
        public int IdPergunta { get; set; }
    }
}