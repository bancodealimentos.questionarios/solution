using System;
using System.Collections.Generic;
using System.Linq;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using repository.Context;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe CategoriaController
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class CategoriaController:Controller
    {
        private IBaseRepository<CategoriaDomain> _categoriaRepository;
        private QuestionariosContext _context;

        /// <summary>
        /// Construtor da classe CategoriaController
        /// </summary>
        /// <param name="categoriaRepository">Retorna o contexto da tabela Categorias</param>
        public CategoriaController(IBaseRepository<CategoriaDomain> categoriaRepository, QuestionariosContext context){
            _categoriaRepository = categoriaRepository;
            _context = context;
        }

        /// <summary>
        /// Retorna todas as categorias
        /// </summary>
        /// <returns>Retorna Json com a lista de todas as categorias</returns>
        /// <response code="200">Retorna uma lista com todas as categorias</response>
        /// <response code="400">Ocorreu um erro</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(List<CategoriaDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult GetCategorias(){
            try{
                return Ok(Json(_categoriaRepository.Listar()));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retorna Lista de Categorias de Questionários
        /// </summary>
        /// <returns>Retorna Json com as categorias dos questionários</returns>
        /// <response code="200">Retorna Json com as categorias dos questionários</response>
        /// <response code="400">Ocorreu um erro</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador")]
        [Route("questionarios")]
        [ProducesResponseType(typeof(List<CategoriaDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult GetCategoriasQuestionario(){
            try{
                var respostaJson = _context.Categorias.Where(c => c.TipoCategoria == "Questionários");
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna Lista de Categorias de Perguntas
        /// </summary>
        /// <returns>Retorna Json com as categorias das perguntas</returns>
        /// <response code="200">Retorna Json com as categorias dos questionários</response>
        /// <response code="400">Ocorreu um erro</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador")]
        [Route("perguntas")]
        [ProducesResponseType(typeof(List<CategoriaDomain>), 200)]
        [ProducesResponseType(typeof(List<string>), 400)]
        public IActionResult GetCategoriasPergunta(){
            try{
                var respostaJson = _context.Categorias.Where(c => c.TipoCategoria == "Perguntas");
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna uma Categoria pelo Id indicado
        /// </summary>
        /// <param name="id">Id da cagegoria</param>
        /// <returns>Retorna uma categoria pelo id indicado</returns>
        /// <response code="200">Retorna um Json com a categoria indicada pelo id</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Categoria não encontrada</response>
        [HttpGet("{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(CategoriaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetCategoriaId(int id){
            try{
                return Ok(Json(_categoriaRepository.BuscarPorId(id)));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }        
    }
}