using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace domain.Entities
{
    /// <summary>
    /// Classe TipoPerguntaDomain - Herda características de BaseDomain
    /// </summary>
    public class TipoPerguntaDomain:BaseDomain
    {
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public String Nome { get; set; }

        public ICollection<PerguntaDomain> Perguntas { get; set; }
    }
}