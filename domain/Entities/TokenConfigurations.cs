namespace domain.Entities
{
    /// <summary>
    /// Classe TokenConfigurations - Configurações para o JWT
    /// </summary>
    public class TokenConfigurations
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
}