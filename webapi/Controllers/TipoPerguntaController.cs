using System;
using System.Collections.Generic;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe TipoPerguntaController
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class TipoPerguntaController:Controller
    {
        private IBaseRepository<TipoPerguntaDomain> _tipoPerguntaRepository;

        /// <summary>
        /// Construtor da classe TipoPerguntaController
        /// </summary>
        /// <param name="tipoPerguntaRepository">Retorna o contexto da tabela TiposPerguntas</param>
        public TipoPerguntaController(IBaseRepository<TipoPerguntaDomain> tipoPerguntaRepository){
            _tipoPerguntaRepository = tipoPerguntaRepository;
        }

        /// <summary>
        /// Retorna os tipos de perguntas
        /// </summary>
        /// <returns>Retorna resposta Json com a lista dos tipos de perguntas</returns>
        /// <response code="200">Retorna uma lista dos Tipos de perguntas</response>
        /// <response code="400">Ocorreu um erro</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(List<TipoPerguntaDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult GetTiposPerguntas(){
            try{
                return Ok(Json(_tipoPerguntaRepository.Listar()));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retorna um tipo de pergunta pelo seu id
        /// </summary>
        /// <param name="id">Id do tipo de pergunta</param>
        /// <returns>Retorna um tipo de pergunta pelo seu id</returns>
        /// <response code="200">Retorna um tipo de pergunta pelo seu id</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Tipod de pergunta não encontrado</response>
        [HttpGet("{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(TipoPerguntaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetTipoPerguntaId(int id){
            try{
                return Ok(Json(_tipoPerguntaRepository.BuscarPorId(id)));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }


    }
}