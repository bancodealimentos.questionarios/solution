using System;
using System.Collections.Generic;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe PermissaoController
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class PermissaoController:Controller
    {
        private IBaseRepository<PermissaoDomain> _permissaoRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissaoRepository"></param>
        public PermissaoController(IBaseRepository<PermissaoDomain> permissaoRepository){
            _permissaoRepository = permissaoRepository;
        }

        /// <summary>
        /// Retorna uma lista de permissões
        /// </summary>
        /// <returns>Json com as permissões</returns>
        /// <response code="200">Retorna uma lista de permissões</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Permissões não encontradas</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(List<PermissaoDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        
        public IActionResult GetPermissoes(){
            try{
                return Ok(Json(_permissaoRepository.Listar()));
            }catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retorna uma Permissão pelo id
        /// </summary>
        /// <param name="id">Id da permissão</param>
        /// <returns>Retorna resposta Json com a permissão indicada pelo id</returns>
        /// <response code="200">Retorna uma Permissão pelo id</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Permissão não encontrada</response>
        [HttpGet("id")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(PermissaoDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetPermissaoId(int id){
            try{
                return Ok(Json(_permissaoRepository.BuscarPorId(id)));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }
    }
}