using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Casse BaseDomain - Superclasse com os atributos principais
    /// </summary>
    public class BaseDomain
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        //[Required]
        [DataType(DataType.DateTime)]
        public DateTime DataCriacao { get; set; } = DateTime.Now;

        //[Required]
        [DefaultValue(false)]
        public Boolean Deletado { get; set; } = false;
    }
}