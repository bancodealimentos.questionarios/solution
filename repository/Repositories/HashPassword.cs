using System.Text;
using domain.Entities;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;

namespace repository.Repositories
{
    public class HashPassword
    {

        public static string getHash(UsuarioDomain usuario){
            var salt = Encoding.ASCII.GetBytes(usuario.DataCriacao.ToString());
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: usuario.Senha,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 5000,
                    numBytesRequested: 256/8
            ));
            return hashed;
        }
    }
}