using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe QuestionarioPerguntaDomain - Herda características de BaseDomain
    /// </summary>
    public class QuestionarioPerguntaDomain:BaseDomain
    {
        [Required]
        public Boolean Ativo { get; set; } = true;

        [ForeignKey("IdQuestionario")]
        public QuestionarioDomain Questionario { get; set; }
        public int IdQuestionario { get; set; }

        [ForeignKey("IdPergunta")]
        public PerguntaDomain Pergunta { get; set; }
        public int IdPergunta { get; set; }

        public virtual ICollection<RespostaDomain> Respostas { get; set; }
    }
}