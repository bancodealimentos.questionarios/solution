using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using domain.Entities;

namespace webapi.ViewModels
{
    /// <summary>
    /// Classe QuestionarioViewModel - Usada para receber os dados para cadastro de questionários
    /// </summary>
    public class QuestionarioViewModel
    {
        /// <value>Id do Questionário</value>
        public int Id { get; set; }

        /// <value>Nome do Questionário</value>
        [Required]
        [StringLength(100, MinimumLength = 4)]
        public String Nome { get; set; } 

        /// <value>Questionário ativo ou não</value>
        [Required]
        public Boolean Ativo { get; set; } = true;

        /// <value>Questionário privado ou público</value>
        [Required]
        public Boolean Privado { get; set; } = false;

        /// <value>Senha para acesso ao questionário</value>
        [StringLength(10, MinimumLength = 4)]
        public String Senha { get; set; }

        /// <value>Usuário que criou o Questionário</value>
        [ForeignKey("IdUsuario")]
        public UsuarioDomain Usuario { get; set; }
        
        /// <value>Id do Usuário que criou o Questionário</value>
        public int IdUsuario { get; set; }
        
        /// <value>Categoria do Questionário</value>
        [ForeignKey("IdCategoria")]
        public CategoriaDomain Categoria { get; set; }
        
        /// <value>Id da Categoria do Questionário</value>
        [Required]
        public int IdCategoria { get; set; }

        /// <value>Lista de inteiros representados pelos Ids das Perguntas incluídas no Questionário</value>
        public List<int> Perguntas { get; set; }
    }
}