using System;
using System.Collections.Generic;
using System.Linq;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using repository.Context;
using webapi.ViewModels;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe PerguntaController
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class PerguntaController:Controller
    {
        private IBaseRepository<PerguntaDomain> _perguntaRepository;
        public QuestionariosContext _context { get; set; }

        /// <summary>
        /// Construtor da classe PerguntaController
        /// </summary>
        /// <param name="perguntaRepository">Retorna o contexto da tabela Perguntas</param>
        public PerguntaController(IBaseRepository<PerguntaDomain> perguntaRepository, QuestionariosContext context){
            _perguntaRepository = perguntaRepository;
            _context = context;
        }

        /// <summary>
        /// Retorna uma pergunta com suas configurações e alternativas (quando possuir).
        /// </summary>
        /// <param name="id">Id da pergunta</param>
        /// <returns>Retorna uma pergunta com suas configurações e alternativas (quando possuir)</returns>
        /// <response code="200">Retorna uma pergunta com suas configurações e alternativas (quando possuir)</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Pergunta não encontrada</response>
        [HttpGet("{id}")]
        [Authorize("Bearer",Roles="Administrador,Palestrante,Monitor")]
        [ProducesResponseType(typeof(PerguntaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetPergunta(int id){
            //var pergunta = _perguntaRepository.BuscarPorId(id, new string[]{"Usuario", "Categoria", "PerguntaConfiguracoes", "Alternativas", "TipoPergunta"});
            try{
            
                var pergunta = _context.Perguntas.Where(p => p.Id == id && p.Deletado == false).Include("Usuario").Include("Categoria").Include("PerguntaConfiguracoes").Include("Alternativas").Include("TipoPergunta").FirstOrDefault();

                if(pergunta == null){
                    return Ok(Json("Pergunta Deletada"));
                }
                
                var respostaJsons = new {
                    pergunta.Id,
                    pergunta.Pergunta,
                    categoria = pergunta.Categoria.Nome,
                    pergunta.Ativo,
                    pergunta.DataCriacao,
                    usuariop = pergunta.Usuario.Nome,
                    tipoPergunta = pergunta.TipoPergunta.Nome,
                    configuracoes = pergunta.PerguntaConfiguracoes.Select(c => new {
                        c.Id,
                        c.Campo,
                        c.Valor,
                        c.TipoValor,
                        c.DataCriacao
                    }),
                    Alternativas = pergunta.Alternativas.Select(a => new{
                        a.Id,
                        a.Alternativa,
                        a.DataCriacao,
                    })
                };
                return Ok(respostaJsons);
            } catch (Exception ex){
                throw new Exception(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna uma pergunta com suas configurações, alternativas e respostas (quando possuir).
        /// </summary>
        /// <param name="id">Id da pergunta</param>
        /// <returns>Retorna uma pergunta com suas configurações, alternativas e respostas (quando possuir)</returns>
        /// <response code="200">Retorna uma pergunta com suas configurações e alternativas e respostas (quando possuir)</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Pergunta não encontrada</response>
        [HttpGet("respostas/{id}")]
        [Authorize("Bearer",Roles="Administrador,Palestrante,Monitor")]
        [ProducesResponseType(typeof(PerguntaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetPerguntaComRespostas(int id){
            try{
                var pergunta = _perguntaRepository.BuscarPorId(id, new string[]{"Usuario", "Categoria", "PerguntaConfiguracoes", "Alternativas", "QuestionariosPerguntas", "QuestionariosPerguntas.Respostas", "TipoPergunta"});
                /*var pergunta = _context.Perguntas.Where(p => p.Id == id && p.Deletado == false).Include("Usuario").Include("Categoria")
                                            .Include("PerguntaConfiguracoes").Include("Alternativas").Include("TipoPergunta")
                                            .Include("QuestionariosPerguntas").Include("Respostas").FirstOrDefault();*/

                if(pergunta.Deletado == true){
                    return BadRequest("A pergunta está deletada.");
                }

                var respostaJson = new {
                    pergunta.Id,
                    pergunta.Pergunta,
                    categoria = pergunta.Categoria.Nome,
                    pergunta.Ativo,
                    pergunta.DataCriacao,
                    usuariop = pergunta.Usuario.Nome,
                    tipoPergunta = pergunta.TipoPergunta.Nome,
                    configuracoes = pergunta.PerguntaConfiguracoes.Select(c => new{
                        c.Id,
                        c.Campo,
                        c.Valor,
                        c.TipoValor,
                        c.DataCriacao
                    }),
                    Alternativas = pergunta.Alternativas.Select(a => new{
                        a.Id,
                        a.Alternativa,
                        a.DataCriacao,
                    }),
                    respostas = pergunta.QuestionariosPerguntas.Select(qp => new{
                        qp.Respostas
                    })
                };
                return Ok(respostaJson);
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Retorna lista de perguntas com os dados resumidos 
        /// </summary>
        /// <returns>Retorna lista de perguntas com os dados resumidos</returns>
        /// <response code="200">Retorna uma lista de perguntas</response>
        /// <response code="400">Ocorreu um erro</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador,Palestrante,Monitor")]
        [Route("detalhes")]
        [ProducesResponseType(typeof(List<PerguntaDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult GetPerguntasDetalhes(){
            try{
                var perguntasDetalhes = _perguntaRepository.Listar(new string[]{"QuestionariosPerguntas", "Usuario", "Categoria", "QuestionariosPerguntas.Respostas", "TipoPergunta"}).Where(p => p.Deletado == false);
                /*var perguntasDetalhes = _context.Perguntas.Where(p => p.Deletado == false).Include("QuestionariosPerguntas")
                                .Include("Usuario").Include("Categoria").Include("TipoPergunta");*/
                var respostaJson = perguntasDetalhes.Select(x => new{
                        x.Id,
                        x.Pergunta,
                        x.Ativo,
                        categoria = x.Categoria.Nome,
                        usuario = x.Usuario.Nome,
                        tipo = x.TipoPergunta.Nome,
                        qtdQuestionarios = x.QuestionariosPerguntas.Select(qq => new{
                            qq.Questionario
                        }).Count(),
                        respostas = x.QuestionariosPerguntas.GroupBy(r => new{
                            r.Respostas
                        }).Count(),
                        x.DataCriacao
                    }).ToArray();
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Cadastro de Pergunta
        /// </summary>
        /// <param name="pergunta">Objeto Pergunta</param>
        /// <returns></returns>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para cadastrar a Pergunta request:
        /// 
        ///     POST /Pergunta
        ///     {
        ///         "idtipopergunta": 2,
        ///         "idcategoria": 4,
        ///         "pergunta": "Pergunta multipla escolha varias alternativas?",
        ///         "idusuario": 1,
        ///         "perguntaConfiguracoes": [
        ///             {
        ///                 "campo": "Min",
        ///                 "valor": "1",
        ///                 "tipoValor": "Inteiro",
        ///             },
        ///             {
        ///                 "campo": "Max",
        ///                 "valor": "2",
        ///                 "tipoValor": "Inteiro",
        ///             }
        ///          ],
        ///         "alternativas": [
        ///             {
        ///                 "alternativa": "Alternativa 1",
        ///             },
        ///             {
        ///                 "alternativa": "Alternativa 2",
        ///             },
        ///             {
        ///                 "alternativa": "Alternativa 3",
        ///             },
        ///             {
        ///                 "alternativa": "Alternativa 4",
        ///             },
        ///             {
        ///                 "alternativa": "Alternativa 5",
        ///             }
        ///         ]
        ///     }    
        /// </remarks>
        /// <response code ="200">Cadastro de pergunta efetuado com sucesso!"</response>
        /// <response code ="400">Ocorreu um erro ao tentar cadastrar.</response>
        [HttpPost]
        [Authorize("Bearer",Roles="Administrador,Palestrante")]
        [ProducesResponseType(typeof(PerguntaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Cadastro([FromBody] PerguntaDomain pergunta){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try{
                pergunta = _perguntaRepository.Inserir(pergunta);
                return Ok(Json("Realizado com sucesso"));
            } catch (Exception ex){
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Atualiza a pergunta indicada
        /// </summary>
        /// <remarks>
        /// Modelo da pergunta que irá ser atualizada, request:
        ///     PUT /Pergunta
        ///     {
        ///         "id" : 0,
        ///         "ativo" : true,
        ///     }
        /// </remarks>
        /// <param name="pergunta">Objeto pergunta completo</param>
        /// <returns>Resposta Json</returns>
        /// <response code="200">Retorna mensagem de sucesso</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Pergunta não encontrada</response>
        [HttpPut]
        [Authorize("Bearer",Roles="Administrador")]
        [Route("atualizar")]
        [ProducesResponseType(typeof(PerguntaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult Atualizar([FromBody] PerguntaViewModel perguntaInformada){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try{

                if(perguntaInformada.Id != 0){
                    PerguntaDomain pergunta = _perguntaRepository.BuscarPorId(perguntaInformada.Id);
                    pergunta.Ativo = perguntaInformada.Ativo;
                    return Ok(_perguntaRepository.Atualizar(pergunta));
                } else {
                    return BadRequest("Os dados da pergunta não estão completos para a atualização");
                }

            } catch(System.Exception ex){
                return BadRequest(ex.Message);
            } 
        }

        /*public IActionResult Atualizar([FromBody] PerguntaDomain pergunta){
            try{
                if(pergunta == null){
                    return BadRequest("Os dados da pergunta não estão completos para a atualização");
                }
                return Ok(_perguntaRepository.Atualizar(pergunta));
            } catch(System.Exception ex){
                return BadRequest(ex.Message);
            }
        }*/

        /// <summary>
        /// Deleta a pergunta informada
        /// </summary>
        /// <param name="id">Id da pergunta</param>
        /// <returns>Retorna a resposta</returns>
        /// <response code="200">Retorna mensagem de sucesso</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Pergunta não encontrada</response>
        [HttpDelete("{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(PerguntaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult Delete(int id){
            try{
                PerguntaDomain pergunta = _perguntaRepository.BuscarPorId(id);
                pergunta.Deletado = true;
                int retorno = _perguntaRepository.Deletar(pergunta);
                if(retorno == 1){
                    return Ok("Pergunta deletada com sucesso");
                } else {
                    return BadRequest("Não foi possível deletar a pergunta");
                }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            }
        }

    }
}