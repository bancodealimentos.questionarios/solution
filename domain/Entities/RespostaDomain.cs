using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe RespostaDomain - Herda características de BaseDomain
    /// </summary>
    public class RespostaDomain:BaseDomain
    {
        [Required]
        [StringLength(1000)]
        public String Resposta { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public String TipoValor { get; set; }

        //[Required]
        [StringLength(100)]
        public string Sessao { get; set; }

        [ForeignKey("IdQuestionarioPergunta")]
        public QuestionarioPerguntaDomain QuestionarioPergunta { get; set; }
        public int IdQuestionarioPergunta { get; set; }

    }
}