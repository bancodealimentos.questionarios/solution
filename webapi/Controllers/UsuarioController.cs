using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using repository.Context;
using repository.Repositories;
using webapi.ViewModels;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe UsuarioController
    /// </summary>
    
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class UsuarioController:Controller
    {

        //UsuarioDomain usuario = new UsuarioDomain();
        private IBaseRepository<UsuarioDomain> _usuarioRepository;
        private QuestionariosContext _context;

        /// <summary>
        /// Construtor da classe UsuarioController
        /// </summary>
        /// <param name="usuarioRepository">Retorna o contexto da tabela Usuarios</param>
        public UsuarioController(IBaseRepository<UsuarioDomain> usuarioRepository, QuestionariosContext context){
            _usuarioRepository = usuarioRepository;
            _context = context;
        }

        /// <summary>
        /// Cadastro de Usuário
        /// </summary>
        /// <param name="usuario">Objeto Usuario</param>
        /// <returns></returns>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para cadastrar o usuario request:
        /// 
        ///     POST /Usuario
        ///     {
        ///         "nome" : "nome do usuário",
        ///         "usuario" : "usuario login",
        ///         "senha" : "senha",
        ///         "email" : "email",
        ///         "ativo" : 1,
        ///         "idpermissao" : 2
        ///     }    
        /// </remarks>
        /// <response code ="200">Cadastro de usuário efetuado com sucesso!"</response>
        /// <response code ="400">Ocorreu um erro ao tentar cadastrar.</response>
        [HttpPost]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Cadastro([FromBody] UsuarioDomain usuario){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try{
                //var username = _usuarioRepository.Listar().FirstOrDefault(u => u.Usuario.Equals(usuario.Usuario));
                var username = _context.Usuarios.FirstOrDefault(u => u.Usuario.Equals(usuario.Usuario));
                if(username == null){
                    usuario.DataCriacao = DateTime.Now;
                    usuario.Senha = HashPassword.getHash(usuario);
                    var retorno = _usuarioRepository.Inserir(usuario);
                    if(retorno.Id != 0){
                        return Ok(retorno);
                    } else {
                        return BadRequest("Cadastro não efetuado.");
                    }
                } else {
                    return BadRequest("O nome de Usuário já existe no banco.");
                }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna lista de usuários para tela de gerência 
        /// </summary>
        /// <returns>Retorna lista de usuários</returns>
        /// <response code="200">Retorna uma lista de usuários</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Usuários não encontrados</response>
        [HttpGet]
        [Authorize("Bearer",Roles="Administrador")]
        [Route("detalhes")]
        [ProducesResponseType(typeof(List<UsuarioDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetUsuariosDetalhes(){
            try{
                //var usuariosDetalhes = _usuarioRepository.Listar(new string[]{"Permissao", "Questionarios", "Perguntas"}).Where(d => d.Deletado == false);
                var usuariosDetalhes = _context.Usuarios.Where(d => d.Deletado == false).Include("Questionarios").Include("Perguntas");
                
                var respostaJson = usuariosDetalhes.Select(x => new{
                        x.Id,
                        x.Nome,
                        x.Senha,
                        x.Email,
                        x.Ativo,
                        Permissao = x.Permissao.Nome,
                        QtdQuestionarios = x.Questionarios.Select(q => new {
                            q.IdUsuario
                        }).Count(),
                        QtdPerguntas = x.Perguntas.Select(p => new {
                            p.IdUsuario
                        }).Count(),
                        x.DataCriacao
                    }).ToArray();
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna um usuário detalhado
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Retorna um usuário detalhado</returns>
        /// <response code="200">Retorna um usuário detalhado</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Usuário não encontrado</response>
        [HttpGet("detalhes/{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetUsuarioDetalhes(int id){
           try{
                //var usuario = _usuarioRepository.BuscarPorId(id, new string[]{"Permissao", "Questionarios", "Questionarios.QuestionariosPerguntas.Pergunta"});
                var usuario = _context.Usuarios.Where(u => u.Id == id && u.Deletado == false).Include("Permissao").Include("Questionarios").Include("Perguntas").FirstOrDefault();
                
                if(usuario == null){
                    return BadRequest(Json("Usuário não existe ou foi deletado"));
                }

                var respostaJson = new{
                    usuario.Id,
                    usuario.Nome,
                    usuario.Usuario,
                    usuario.Email,
                    usuario.Ativo,
                    Permissao = usuario.Permissao.Nome,
                    QtdQuestionarios = usuario.Questionarios.Select(q => new {
                        q.IdUsuario
                    }).Count(),
                    QtdPerguntas = usuario.Perguntas.Select(p => new {
                        p.IdUsuario
                    }).Count(),
                    usuario.DataCriacao
                };
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna um usuário pelo ID
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Retorna um usuário pelo ID</returns>
        /// <response code="200">Retorna um usuário</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Usuário não encontrado</response>
        [HttpGet("{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetUsuario(int id){
           try{
                var usuario = _usuarioRepository.BuscarPorId(id);

                if(usuario.Deletado == true){
                    return Ok(Json("Usuário Deletado"));
                }
                
                var respostaJson = new{
                    usuario.Id,
                    usuario.Nome,
                    usuario.Usuario,
                    usuario.Email,
                    usuario.Ativo,
                    usuario.IdPermissao,
                    usuario.DataCriacao,
                };
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Atualiza o usuário indicado
        /// </summary>
        /// <remarks>
        /// Modelo do usuário que irá ser atualizado, request:
        ///     PUT /Usuario
        ///     {
        ///         "id" : 0,
        ///         "nome" : "Nome completo do usuário",
        ///         "usuario" : "nome de usuário para login",
        ///         "senha" : "senha do usuário",
        ///         "email" : "email do usuário",
        ///         "ativo" : 1,
        ///         "idpermissao" : 3
        ///     }
        /// </remarks>
        /// <param name="usuario">Objeto usuário completo</param>
        /// <returns>Resposta Json</returns>
        /// <response code="200">Retorna mensagem de sucesso</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Usuário não encontrado</response>
        [HttpPut]
        [Authorize("Bearer",Roles="Administrador")]
        [Route("atualizar")]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult Atualizar([FromBody] UsuarioViewModel usuarioInformado){
            try{
                if (!ModelState.IsValid){
                    var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                    return BadRequest(Json(errorList));
                }
                if(usuarioInformado.Id != 0){                    
                    UsuarioDomain usuario = _usuarioRepository.BuscarPorId(usuarioInformado.Id);
                    if(usuarioInformado.Senha != null){
                        usuario.Senha = usuarioInformado.Senha;
                        usuario.Senha = HashPassword.getHash(usuario);
                    }
                    usuario.Nome = usuarioInformado.Nome;
                    usuario.Usuario = usuarioInformado.Usuario;
                    usuario.IdPermissao = usuarioInformado.IdPermissao;
                    usuario.Email = usuarioInformado.Email;
                    usuario.Ativo = usuarioInformado.Ativo;
                    return Ok(_usuarioRepository.Atualizar(usuario));
                } 
                return BadRequest("Os dados do usuário não estão completos para a atualização");
            } catch(System.Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deleta o usuário informado
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Retorna a resposta</returns>
        /// <response code="200">Retorna mensagem de sucesso</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Usuário não encontrado</response>
        [HttpDelete("{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult Delete(int id){
            try{
                UsuarioDomain usuario = _usuarioRepository.BuscarPorId(id);
                usuario.Deletado = true;
                int retorno = _usuarioRepository.Deletar(usuario);
                if(retorno == 1){
                    return Ok("Usuario Deletado com sucesso");
                } else {
                    return BadRequest("Não foi possível deletar o usuário");
                }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            }
        }
    }
}