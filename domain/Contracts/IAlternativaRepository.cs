using domain.Entities;

namespace domain.Contracts
{
    public interface IAlternativaRepository : IBaseRepository<AlternativaDomain>
    {
         int Excluir(AlternativaDomain alternativa);
    }
}