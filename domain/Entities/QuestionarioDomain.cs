using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe QuestionarioDomain - Herda características de BaseDomain
    /// </summary>
    public class QuestionarioDomain:BaseDomain
    {
        [Required]
        [StringLength(100, MinimumLength = 4)]
        public String Nome { get; set; }

        [Required]
        public Boolean Ativo { get; set; } = true;

        [Required]
        public Boolean Privado { get; set; } = false;

        [StringLength(10, MinimumLength = 4)]
        public String Senha { get; set; }

        [ForeignKey("IdUsuario")]
        public UsuarioDomain Usuario { get; set; }
        public int IdUsuario { get; set; }
        
        [ForeignKey("IdCategoria")]
        public CategoriaDomain Categoria { get; set; }

        public int IdCategoria { get; set; }

        public ICollection<QuestionarioPerguntaDomain> QuestionariosPerguntas { get; set; }
    }
}