﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using repository.Context;
using domain.Contracts;
using repository.Repositories;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using domain.Entities;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace webapi
{
    /// <summary>
    /// Inicia as configurações necessárias para a execução do projeto
    /// </summary>
    public class Startup
    {
        /// <value>Atributo que receberá as configurações do aplicativo</value>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Carrega os valores da configuração
        /// </summary>
        /// <param name="configuration">Valores da configuração</param>
        public Startup(IConfiguration configuration){
            this.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        /// <summary>
        /// Adiciona os serviços no container do injetor de dependências
        /// Serviçõs adicionados: Autenticação com JWT, Contexto do Banco de dados, Arquitetura MVC,
        ///  Opções Json, Escopo, Liberar o Cors e Documentação com Swagger
        /// </summary>
        /// <param name="services">Serviços a serem adicionados</param>
        public void ConfigureServices(IServiceCollection services)
        {

            var signingConfigurations = new SigningConfigurations();
                services.AddSingleton(signingConfigurations);

                var tokenConfigurations = new TokenConfigurations();
                new ConfigureFromConfigurationOptions<TokenConfigurations>(
                    Configuration.GetSection("TokenConfigurations"))
                        .Configure(tokenConfigurations);
                services.AddSingleton(tokenConfigurations);


                services.AddAuthentication(authOptions =>
                {
                    authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(bearerOptions =>
                {
                    var paramsValidation = bearerOptions.TokenValidationParameters;
                    paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                    paramsValidation.ValidAudience = tokenConfigurations.Audience;
                    paramsValidation.ValidIssuer = tokenConfigurations.Issuer;

                    paramsValidation.ValidateIssuerSigningKey = true;

                    paramsValidation.ValidateLifetime = true;

                    paramsValidation.ClockSkew = TimeSpan.Zero;
                });

                services.AddAuthorization(auth =>
                {
                    auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser().Build());
                });

            services.AddDbContext<QuestionariosContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnectionSmartasp")));
            
            services
            .AddMvc(options => {options.Filters.Add(new CorsAuthorizationFilterFactory("AllowCors"));})
            .AddJsonOptions(options => {
                                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });

            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddCors(o => o.AddPolicy("AllowCors", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                    //.AllowCredentials();
            }));

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info{
                    Version = "V1",
                    Title = "Questionários Api",
                    Description = "Api para gestão e exibição dos questionários",
                    TermsOfService = "None",
                    Contact = new Contact{Name = "Alander Marques Machado", Email = "alandermm@hotmail.com", Url = "http://www.alander.com.br"}
                });

                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "webapi.xml");

                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// <summary>
        /// Configura o uso dos serviços.
        /// </summary>
        /// <param name="app">Aplicativo</param>
        /// <param name="env">Ambiente</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseCors("AllowCors");
            app.UseSwagger();

            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1");
            });
        }
    }
}