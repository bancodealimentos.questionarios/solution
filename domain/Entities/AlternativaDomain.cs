using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe AlternativaDomain - Herda características de BaseDomain
    /// </summary>
    public class AlternativaDomain:BaseDomain
    {
        [Required]
        [StringLength(400, MinimumLength = 1)]
        public String Alternativa { get; set; }

        [Required]
        public Boolean Correta { get; set; }

        [ForeignKey("IdPergunta")]
        public PerguntaDomain Pergunta { get; set; }
        public int IdPergunta { get; set; }
    }
}