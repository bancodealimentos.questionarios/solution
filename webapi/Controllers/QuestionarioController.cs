using System;
using System.Collections.Generic;
using System.Linq;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using repository.Context;
using webapi.ViewModels;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe QuestionarioController
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class QuestionarioController:Controller
    {
        private IBaseRepository<QuestionarioDomain> _questionarioRepository;
        private IBaseRepository<QuestionarioPerguntaDomain> _questionarioPerguntaRepository;
        private QuestionariosContext _context;

        /// <summary>
        /// Construtor da classe QuestionarioController
        /// </summary>
        /// <param name="questionarioRepository">Retorna o contexto da tabela Questionarios</param>
        /// <param name="questionarioPerguntaRepository">Retorna o contexto da tabela QuestionariosPerguntas</param>
        public QuestionarioController(IBaseRepository<QuestionarioDomain> questionarioRepository,
                            IBaseRepository<QuestionarioPerguntaDomain> questionarioPerguntaRepository,
                            QuestionariosContext context){
            _questionarioRepository = questionarioRepository;
            _questionarioPerguntaRepository = questionarioPerguntaRepository;
            _context = context;
        }

        /// <summary>
        /// Retorna lista de questionários com dados resumidos 
        /// </summary>
        /// <returns>Retorna lista de questionários com dados resumidos</returns>
        /// <response code="200">Retorna uma lista de questionários</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Questionários não encontrados</response>
        [HttpGet]
        [Route("detalhes")]
        [Authorize("Bearer",Roles="Administrador,Palestrante,Monitor")]
        [ProducesResponseType(typeof(List<QuestionarioDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetQuetionarioDetalhes(){
            //return Ok(_questionarioRepository.Listar(new string[]{}));
            try{
                var questionarioDetalhes = _questionarioRepository.Listar(new string[]{"QuestionariosPerguntas", "Usuario", "Categoria", "QuestionariosPerguntas.Respostas"}).Where(d => d.Deletado == false);
                //var questionarioDetalhes = _context.Questionarios.Where(d => d.Deletado == false).Include("QuestionariosPerguntas").Include("Usuario").Include("Categoria");
                
                var respostaJson = questionarioDetalhes.Select(x => new{
                        x.Id,
                        x.Nome,
                        x.Ativo,
                        categoria = x.Categoria.Nome,
                        usuario = x.Usuario.Nome,
                        x.Privado,
                        qtdPerguntas = x.QuestionariosPerguntas.Select(p => new{
                            p.IdQuestionario,
                        }).Count(),
                        respostas = x.QuestionariosPerguntas.Select(r => new{
                            //qtd = r.Respostas.Sum(c => c.QuestionarioPergunta.Pergunta.Id == r.Respostas.FirstOrDefault().QuestionarioPergunta.IdPergunta),
                        }).Count(),
                        x.DataCriacao
                    }).ToArray();
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retorna lista de questionários para a tela do cliente 
        /// </summary>
        /// <returns>Retorna lista de questionários para a tela do cliente</returns>
        /// <response code="200">Retorna uma lista de questionários para a tela de clientes</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Questionários não encontrados</response>
        [HttpGet]
        [AllowAnonymous]
        [Route("listacliente")]
        [ProducesResponseType(typeof(List<QuestionarioDomain>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetQuetionariosCliente(){
            //return Ok(_questionarioRepository.Listar(new string[]{}));
            try{
                var questionarioDetalhes = _questionarioRepository.Listar(new string[]{"Categoria"}).Where(d => d.Deletado == false && d.Ativo == true);
                //var questionarioDetalhes = _context.Questionarios.Where(d => d.Deletado == false && d.Ativo == true).Include("Categorias");
                
                //var preJson = questionarioDetalhes.Where(x => x.Ativo == true && x.Deletado == false);
                
                var respostaJson = questionarioDetalhes.Select(x => new{
                        x.Id,
                        x.Nome,
                        categoria = x.Categoria.Nome,
                        x.Privado,
                    }).ToArray();
                return Ok(Json(respostaJson));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retorna um questionário com suas perguntas, configurações das perguntas e alternativas.
        /// </summary>
        /// <param name="id">Id do questionário</param>
        /// <returns>Retorna um questionário com suas perguntas e alternativas</returns>
        /// <response code="200">Retorna um questionário com suas perguntas, configurações das perguntas e alternativas</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Questionário não encontrado</response>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetQuestionario(int id){
            var questionario = _questionarioRepository.BuscarPorId(id, new string[]{"QuestionariosPerguntas", "Usuario", "Categoria",
                                "QuestionariosPerguntas.Pergunta", "QuestionariosPerguntas.Pergunta.PerguntaConfiguracoes",
                                "QuestionariosPerguntas.Pergunta.Usuario","QuestionariosPerguntas.Respostas",
                                "QuestionariosPerguntas.Pergunta.Alternativas", "QuestionariosPerguntas.Pergunta.TipoPergunta",
                                 "QuestionariosPerguntas.Pergunta.Categoria"
                            });

            /*var questionario = _context.Questionarios.Where(q => q.Id == id && q.Deletado == false).Include("Usuario")
                            .Include("QuestionariosPerguntas").Include("Perguntas").Include("Categoria")
                            .Include("TipoPergunta").Include("PerguntaConfiguracoes").Include("Respostas")
                            .Include("Alternativas").FirstOrDefault();*/
            
            if(questionario == null){
                return Ok(Json("Questionário Deletado"));
            }

            var respostaJson = new {
                questionario.Id,
                questionario.Nome,
                categoria = questionario.Categoria.Nome,
                questionario.Ativo,
                questionario.DataCriacao,
                usuarioq = questionario.Usuario.Nome,
                questionario.Privado,
                perguntas = questionario.QuestionariosPerguntas.Where(x => x.Deletado == false).Select(d => new {
                    id = d.Pergunta.Id,
                    datacriacao = d.Pergunta.DataCriacao,
                    tipopergunta = d.Pergunta.TipoPergunta.Nome,
                    categoria = d.Pergunta.Categoria.Nome,
                    pergunta = d.Pergunta.Pergunta,
                    usuariop = d.Pergunta.Usuario.Nome,
                    configuracoes = d.Pergunta.PerguntaConfiguracoes.Select(c => new{
                        c.Id,
                        c.Campo,
                        c.Valor,
                        c.TipoValor,
                        c.DataCriacao
                    }),
                    Alternativas = d.Pergunta.Alternativas.Select(a => new{
                        a.Id,
                        a.Alternativa,
                        a.DataCriacao,
                        a.Correta
                    })

                })
            };
            return Ok(respostaJson);
        }

        /// <summary>
        /// Retorna senha para tela de edição de questionários
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("senha/{id}")]
        [Authorize("Bearer",Roles="Administrador,Palestrante")]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetSenhaQuestionario(int id){
            try{
                var questionario = _context.Questionarios.FirstOrDefault(x => x.Id == id && x.Privado == true);
                if(questionario != null){
                    string senha = questionario.Senha.ToString();
                    return Ok(senha);
                } else {
                    return BadRequest("O questionário não tem senha.");
                }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Retorna um questionário completo, com perguntas, configurações das perguntas, alternativas e respostas
        /// </summary>
        /// <param name="id">Id do questionário</param>
        /// <returns>Retorna um questionário completo</returns>
        /// <response code="200">Retorna um questionário completo, com perguntas, configurações das perguntas, alternativas e respostas</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Questionário não encontrado</response>
        [HttpGet("respostas/{id}")]
        [Authorize("Bearer",Roles="Administrador,Palestrante,Monitor")]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetQuestionarioComRespostas(int id){
            var questionario = _questionarioRepository.BuscarPorId(id, new string[]{"QuestionariosPerguntas", "Usuario", "Categoria", "QuestionariosPerguntas.Pergunta", "QuestionariosPerguntas.Pergunta.PerguntaConfiguracoes", "QuestionariosPerguntas.Respostas", "QuestionariosPerguntas.Pergunta.Usuario", "QuestionariosPerguntas.Pergunta.Alternativas", "QuestionariosPerguntas.Pergunta.TipoPergunta", "QuestionariosPerguntas.Pergunta.Categoria"});
            
            /*var questionario = _context.Questionarios.Where(q => q.Id == id && q.Deletado == false).Include("Usuarios")
                            .Include("QuestionariosPerguntas").Include("Perguntas").Include("Categorias")
                            .Include("TipoPergunta").Include("PerguntaConfiguracoes").Include("Respostas")
                            .Include("Alternativas").FirstOrDefault();*/

            var respostaJson = new {
                questionario.Id,
                questionario.Nome,
                categoria = questionario.Categoria.Nome,
                questionario.Ativo,
                questionario.DataCriacao,
                usuarioq = questionario.Usuario.Nome,
                questionario.Privado,
                perguntas = questionario.QuestionariosPerguntas.Select(d => new {
                    id = d.Pergunta.Id,
                    datacriacao = d.Pergunta.DataCriacao,
                    tipopergunta = d.Pergunta.TipoPergunta.Nome,
                    categoria = d.Pergunta.Categoria.Nome,
                    pergunta = d.Pergunta.Pergunta,
                    usuariop = d.Pergunta.Usuario.Nome,
                    configuracoes = d.Pergunta.PerguntaConfiguracoes.Select(c => new{
                        c.Id,
                        c.Campo,
                        c.Valor,
                        c.TipoValor,
                        c.DataCriacao
                    }),
                    Alternativas = d.Pergunta.Alternativas.Select(a => new{
                        a.Id,
                        a.Alternativa,
                        a.DataCriacao,
                    }),
                    respostas = d.Respostas.Select(r => new{
                        r.Id,
                        r.DataCriacao,
                        r.Resposta,
                        r.TipoValor
                    })
                })
            };
            return Ok(respostaJson);
        }


        /// <summary>
        /// Retorna Questionário privado autenticado
        /// </summary>
        /// <param name="questionario">Objeto Questionario</param>
        /// <returns>Retorna o objeto Questionário autenticado</returns>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para autenticar o Questionario privado request:
        /// 
        ///     POST /Questionario
        ///     {
        ///         "id" : 0,
        ///         "senha" : "XYZ123",
        ///     }    
        /// </remarks>
        /// <response code ="200">Questionario autenticado!</response>
        /// <response code ="400">Ocorreu um erro.</response>
        [HttpPost("privado")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Privado([FromBody] QuestionarioDomain questionario){
            try{
                //var retorno = _questionarioRepository.Listar().Where(p => p.Id == questionario.Id && p.Deletado == false && p.Ativo == true);
                var retorno = _context.Questionarios.FirstOrDefault(p => p.Id == questionario.Id && p.Deletado == false && p.Ativo == true);
                string senha = retorno.Senha.ToString();
               if (questionario.Senha.Equals(senha)){
                    return GetQuestionario(questionario.Id);
               }
               else
               {
                    return BadRequest(Json("Senha inválida"));
               }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /// <summary>
        /// Cadastro de Questionário
        /// </summary>
        /// <param name="questionarioPerguntas">Objeto QuestionarioViewModel</param>
        /// <returns></returns>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para cadastrar o Questionarios request:
        /// 
        ///     POST /Questionario
        ///     {
        ///         "nome" : "Questionário 3",
        ///         "ativo" : 1,
        ///         "privado": 1,
        ///         "senha" : "XYZ123",
        ///         "idUsuario": 1,
        ///         "idCategoria": 2,
        ///         "perguntas" : [1,3]
        ///      }    
        /// </remarks>
        /// <response code ="200">Cadastro de questionário efetuado com sucesso!"</response>
        /// <response code ="400">Ocorreu um erro ao tentar cadastrar.</response>
        [HttpPost]
        [Authorize("Bearer",Roles="Administrador,Palestrante")]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Cadastro([FromBody] QuestionarioViewModel questionarioPerguntas){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            QuestionarioDomain questionario;
            List<int> perguntas = questionarioPerguntas.Perguntas.Distinct().ToList();
            List<QuestionarioPerguntaDomain> questionariosPerguntas = new List<QuestionarioPerguntaDomain>();
            try{
                questionario = new QuestionarioDomain(){
                    Nome = questionarioPerguntas.Nome,
                    IdCategoria = questionarioPerguntas.IdCategoria,
                    IdUsuario = questionarioPerguntas.IdUsuario,
                    Ativo = questionarioPerguntas.Ativo,
                    Privado = questionarioPerguntas.Privado,
                    Senha = questionarioPerguntas.Senha,
                };
                questionario = _questionarioRepository.Inserir(questionario);
                foreach (int idPergunta in perguntas ){
                    //questionariosPerguntas.Add(_questionarioPerguntaRepository.Inserir(new QuestionarioPerguntaDomain(){
                    questionariosPerguntas.Add(_questionarioPerguntaRepository.Inserir(new QuestionarioPerguntaDomain(){
                        IdPergunta = idPergunta,
                        IdQuestionario = questionario.Id})
                    );
                }
                return Ok(Json("Realizado com sucesso"));
            } catch (Exception ex){
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Atualiza o questionario indicado
        /// </summary>
        /// <remarks>
        /// Modelo do questionário que irá ser atualizado, request:
        ///     PUT /Questionario
        ///     {
        ///         
        ///     }
        /// </remarks>
        /// <param name="questionario">Objeto questionario completo</param>
        /// <returns>Resposta Json</returns>
        /// <response code="200">Retorna mensagem de sucesso</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Questionario não encontrado</response>
        [HttpPut]
        [Authorize("Bearer",Roles="Administrador")]
        [Route("atualizar")]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult Atualizar([FromBody] QuestionarioViewModel questionarioInformado){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try{
                QuestionarioDomain questionario = _questionarioRepository.BuscarPorId(questionarioInformado.Id);
                if(questionarioInformado != null){
                    questionario.Nome = questionarioInformado.Nome;
                    questionario.Ativo = questionarioInformado.Ativo;
                    questionario.IdCategoria = questionarioInformado.IdCategoria;
                    if(questionarioInformado.Senha != null){
                        questionario.Senha = questionarioInformado.Senha;
                    }
                    questionario.Privado = questionarioInformado.Privado;
                }
                var questionariosPerguntas = _context.QuestionariosPerguntas.Where(x => x.IdQuestionario == questionario.Id && x.Deletado == false);
                List<int> perguntasInformadas = questionarioInformado.Perguntas;
                List<int> perguntasBanco = new List<int>();

                foreach(var qp in questionariosPerguntas){
                    perguntasBanco.Add(qp.IdPergunta);
                }

                var perguntasAdicionadas = perguntasInformadas.Except(perguntasBanco);
                var perguntasRemovidas = perguntasBanco.Except(perguntasInformadas);
                
                foreach (var item in perguntasAdicionadas)
                {
                    QuestionarioPerguntaDomain existe = questionariosPerguntas.FirstOrDefault(x => x.IdPergunta == item);

                    if(existe != null){
                        existe.Deletado = false;
                        _questionarioPerguntaRepository.Atualizar(existe);
                    } else {
                        _questionarioPerguntaRepository.Inserir(new QuestionarioPerguntaDomain(){
                        IdPergunta = item,
                        IdQuestionario = questionario.Id});
                    }
                }
                foreach(var item in perguntasRemovidas){
                    _questionarioPerguntaRepository.Deletar(questionariosPerguntas.FirstOrDefault(p => p.IdPergunta == item));
                }
                
                if(questionario == null){
                    return BadRequest("Os dados do questionário não estão completos para a atualização");
                }
                return Ok(_questionarioRepository.Atualizar(questionario));
            } catch(System.Exception ex){
                return BadRequest(ex.Message);
            } finally {
                _context.Dispose();
            }
        }

        /*public IActionResult Atualizar([FromBody] QuestionarioDomain questionario){
            try{
                if(questionario == null){
                    return BadRequest("Os dados do questionário não estão completos para a atualização");
                }
                return Ok(_questionarioRepository.Atualizar(questionario));
            } catch(System.Exception ex){
                return BadRequest(ex.Message);
            }
        }*/

        /// <summary>
        /// Deleta o questionario informado
        /// </summary>
        /// <param name="id">Id do questionario</param>
        /// <returns>Retorna a resposta</returns>
        /// <response code="200">Retorna mensagem de sucesso</response>
        /// <response code="400">Ocorreu um erro</response>
        /// <response code="404">Questionário não encontrado</response>
        [HttpDelete("{id}")]
        [Authorize("Bearer",Roles="Administrador")]
        [ProducesResponseType(typeof(QuestionarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult Delete(int id){
            try{
                QuestionarioDomain questionario = _questionarioRepository.BuscarPorId(id);
                questionario.Deletado = true;
                int retorno = _questionarioRepository.Deletar(questionario);
                if(retorno == 1){
                    return Ok("Questionário deletado com sucesso");
                } else {
                    return BadRequest("Não foi possível deletar o questionário");
                }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            }
        }
    }
}