using System;
using System.ComponentModel.DataAnnotations;

namespace webapi.ViewModels
{
    /// <summary>
    /// Classe UsuarioViewModel - Usada para receber os dados para login do usuário
    /// </summary>
    public class UsuarioViewModel
    {
        /// <value>Id do Usuario</value>
        public int Id { get; set; }

        /// <value>Nome de usuário</value>
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public String Usuario { get; set; }

        /// <value>Senha do usuário</value>
        [DataType(DataType.Password)]
        [StringLength(60, MinimumLength = 6)]
        public String Senha { get; set; }

        [StringLength(60, MinimumLength = 3)]
        public String Nome { get; set; }

        /// <value>Email do Usuárioalan2167*
        /// </value>
        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public String Email { get; set; }

        /// <value>Usuário ativo ou não</value>
        public Boolean Ativo { get; set; }

        /// <value>Id da Permissão do Usuário</value>
        public int IdPermissao { get; set; }
    }
}