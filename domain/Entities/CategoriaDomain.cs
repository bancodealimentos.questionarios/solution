using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace domain.Entities
{
    /// <summary>
    /// Classe CategoriaDomain - Herda características de BaseDomain
    /// </summary>
    public class CategoriaDomain:BaseDomain
    {
        [Required]
        [StringLength(50, MinimumLength = 4)]
        public String Nome { get; set; }

        [Required]
        [StringLength(50)]
        public String TipoCategoria { get; set; }

        public ICollection<QuestionarioDomain> Questionarios { get; set; }
        public ICollection<PerguntaDomain> Perguntas { get; set; }
        
    }
}