using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using repository.Context;
using repository.Repositories;
using webapi.ViewModels;

namespace webapi.Controllers
{
    /// <summary>
    /// Controlador de Login/Logout
    /// </summary>
    public class LoginController:Controller
    {
        private QuestionariosContext _contextoRepository;

        /// <summary>
        /// Construtor da classe LoginController
        /// </summary>
        /// <param name="contextoRepository">Retorna o contexto do banco de dados</param>
        public LoginController(QuestionariosContext contextoRepository){
            _contextoRepository = contextoRepository;
        }

        /// <summary>
        /// Autentica usuário
        /// </summary>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para autenticar o usuario request:
        /// 
        ///     POST /Usuario
        ///     {
        ///         "usuario" : "username",
        ///         "senha" : "XYZ123",
        ///     }    
        /// </remarks>
        /// <param name="user">Objeto UsuarioViewModel</param>
        /// <param name="signingConfigurations">Configurações de assinaturas</param>
        /// <param name="tokenConfigurations">Configuraçẽos de Token</param>
        /// <response code ="200">Usuário autenticado!</response>
        /// <response code ="400">Ocorreu um erro.</response>
        /// <returns>Retorna autenticação do usuário</returns>
        
        [Route("api/Login")]
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [EnableCors("AllowCors")]
        public object Login([FromBody] UsuarioViewModel user, [FromServices]SigningConfigurations signingConfigurations, [FromServices] TokenConfigurations tokenConfigurations){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try {
                var usuario = _contextoRepository.Usuarios.Include("Permissao").FirstOrDefault(u => u.Usuario.Equals(user.Usuario ) && u.Ativo == true && u.Deletado == false);
                if (usuario == null){
                    return BadRequest("Usuário não existe ou está desativado");
                }
                UsuarioDomain usuarioInformado = new UsuarioDomain(){
                    Usuario = user.Usuario,
                    Senha = user.Senha,
                    DataCriacao = usuario.DataCriacao
                };

                var teste = HashPassword.getHash(usuarioInformado);

                if(usuario.Senha.Equals(HashPassword.getHash(usuarioInformado))){
                    ClaimsIdentity identity = new ClaimsIdentity(new GenericIdentity(usuario.Id.ToString(), "Login"), new[]{
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, usuario.Id.ToString()),
                        new Claim("Nome", usuario.Nome),
                        new Claim("Id", usuario.Id.ToString()),
                        new Claim("Username", usuario.Usuario)
                    });

                    Claim claim = new Claim(ClaimTypes.Role, usuario.Permissao.Nome);
                    identity.AddClaim(claim);

                    var handler = new JwtSecurityTokenHandler();
                    var securityToken = handler.CreateToken(new SecurityTokenDescriptor{
                        Issuer = tokenConfigurations.Issuer,
                        Audience = tokenConfigurations.Audience,
                        SigningCredentials = signingConfigurations.SigningCredentials,
                        Subject = identity
                    });

                    var token = handler.WriteToken(securityToken);

                    var respostaJson = new {
                        usuario.Id,
                        usuario.Nome,
                        usuario.Usuario,
                        permissao = usuario.Permissao.Nome
                    };

                    var retorno = new{autenticacao = true, accessToken = token, message = "OK", usuario = respostaJson};

                    return Ok(retorno);
                }
                var retornoErro = new {autenticacao = false, message = "Falha na Autenticação"};
                return BadRequest(retornoErro);
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Realiza logout do usuário
        /// </summary>
        /// <returns>Logout</returns>
        /// <response code ="200">Usuário deslogado.</response>
        /// <response code ="400">Ocorreu um erro.</response>
        [EnableCors("AllowCors")]
        [Route("api/Logout")]
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(UsuarioDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public object Logout(){
            try{
                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor{
                    Issuer = null,
                    Audience = null,
                    SigningCredentials = null,
                    Subject = null
                });
                var token = handler.WriteToken(securityToken);
                return Ok(Json("Deslogado"));
            } catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }
    }
}