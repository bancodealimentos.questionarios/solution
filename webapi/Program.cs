﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace webapi
{
    /// <summary>
    /// Classe principal da aplicação
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Método que inicia a aplicação
        /// </summary>
        /// <param name="args">argumentos</param>
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        /// <summary>
        /// Constroi aplicação Web
        /// </summary>
        /// <param name="args">argumentos</param>
        /// <returns></returns>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
