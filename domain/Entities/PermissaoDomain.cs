using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace domain.Entities
{
    /// <summary>
    /// Classe PermissaoDomain - Herda características de BaseDomain
    /// </summary>
    public class PermissaoDomain:BaseDomain
    {
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public String Nome { get; set; }

        public ICollection<UsuarioDomain> Usuario { get; set; }
    }
}