using System;
using System.ComponentModel.DataAnnotations;

namespace webapi.ViewModels
{
    public class PerguntaViewModel
    {
        /// <value>Id da Pergunta</value>
        [Required]
        public int Id { get; set; }

        /// <value>Pergunta ativo ou não</value>
        public Boolean Ativo { get; set; }

    }
}