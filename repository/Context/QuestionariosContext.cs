using System.Linq;
using domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace repository.Context
{
    public class QuestionariosContext:DbContext
    {
        public QuestionariosContext(DbContextOptions<QuestionariosContext> options) : base(options){}

        public DbSet<QuestionarioDomain> Questionarios { get; set; }
        public DbSet<QuestionarioPerguntaDomain> QuestionariosPerguntas { get; set; }
        public DbSet<PerguntaDomain> Perguntas { get; set; }
        public DbSet<PerguntaConfiguracaoDomain> PerguntasConfiguracoes { get; set; }
        public DbSet<TipoPerguntaDomain> TiposPerguntas { get; set; }
        public DbSet<CategoriaDomain> Categorias { get; set; }
        public DbSet<AlternativaDomain> Alternativas { get; set; }
        public DbSet<RespostaDomain> Respostas { get; set; }
        public DbSet<UsuarioDomain> Usuarios { get; set; }
        public DbSet<PermissaoDomain> Permissoes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder){

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
                
            modelBuilder.Entity<QuestionarioDomain>().ToTable("Questionarios");
            modelBuilder.Entity<QuestionarioPerguntaDomain>().ToTable("QuestionariosPerguntas");
            modelBuilder.Entity<PerguntaDomain>().ToTable("Perguntas");
            modelBuilder.Entity<PerguntaConfiguracaoDomain>().ToTable("PerguntasConfiguracoes");
            modelBuilder.Entity<TipoPerguntaDomain>().ToTable("TiposPerguntas");
            modelBuilder.Entity<CategoriaDomain>().ToTable("Categorias");
            modelBuilder.Entity<AlternativaDomain>().ToTable("Alternativas");
            modelBuilder.Entity<RespostaDomain>().ToTable("Respostas");
            modelBuilder.Entity<UsuarioDomain>().ToTable("Usuarios");
            modelBuilder.Entity<PermissaoDomain>().ToTable("Permissoes");

            modelBuilder.Entity<UsuarioDomain>().HasIndex(u => u.Usuario).IsUnique().HasName("Usuario");
            
            modelBuilder.Entity<UsuarioDomain>().Property(x => x.Senha).HasMaxLength(60).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}