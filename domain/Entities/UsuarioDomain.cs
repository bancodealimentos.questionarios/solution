using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe UsuarioDomain - Herda características de BaseDomain
    /// </summary>
    public class UsuarioDomain:BaseDomain
    {
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public String Nome { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        public String Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 6)]
        public String Senha { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public String Email { get; set; }

        [Required]
        public Boolean Ativo { get; set; } = true;

        [ForeignKey("IdPermissao")]
        public PermissaoDomain Permissao { get; set; }
        public int IdPermissao { get; set; }

        public ICollection<QuestionarioDomain> Questionarios { get; set; }
        public ICollection<PerguntaDomain> Perguntas { get; set; }
    }
}