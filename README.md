dotnet new solution -o solution
cd solution
dotnet new classlib -o domain
dotnet new classlib -o repository
dotnet new web -o webapi
cd domain
dotnet add package System.ComponentModel.Annotations --version 4.4.1
cd ..\repository\
dotnet add package Microsoft.EntityFrameworkCore --version 2.0.1
dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 2.0.1
dotnet add package Microsoft.EntityFrameworkCore.Relational --version 2.0.1
dotnet sln add "domain/domain.csproj"
dotnet sln add "repository/repository.csproj"
dotnet sln add "webapi/webapi.csproj"
dotnet ef migrations add BancoInicial --startup-project ../webapi/webapi.csproj
dotnet ef database update --startup-project ..\webapi\webapi.csproj
cd ../repository
dotnet add package Microsoft.AspNetCore.Cryptography.KeyDerivation --version 2.1.0-preview1-final

