﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace repository.Migrations
{
    public partial class BancoInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(maxLength: 50, nullable: false),
                    TipoCategoria = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permissoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissoes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposPerguntas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposPerguntas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ativo = table.Column<bool>(nullable: false),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    IdPermissao = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(maxLength: 60, nullable: false),
                    Senha = table.Column<string>(maxLength: 60, nullable: false),
                    Usuario = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Usuarios_Permissoes_IdPermissao",
                        column: x => x.IdPermissao,
                        principalTable: "Permissoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Perguntas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ativo = table.Column<bool>(nullable: false),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    IdCategoria = table.Column<int>(nullable: false),
                    IdTipoPergunta = table.Column<int>(nullable: false),
                    IdUsuario = table.Column<int>(nullable: false),
                    Opcional = table.Column<bool>(nullable: false),
                    Pergunta = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Perguntas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Perguntas_Categorias_IdCategoria",
                        column: x => x.IdCategoria,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Perguntas_TiposPerguntas_IdTipoPergunta",
                        column: x => x.IdTipoPergunta,
                        principalTable: "TiposPerguntas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Perguntas_Usuarios_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Questionarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ativo = table.Column<bool>(nullable: false),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    IdCategoria = table.Column<int>(nullable: false),
                    IdUsuario = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(maxLength: 100, nullable: false),
                    Privado = table.Column<bool>(nullable: false),
                    Senha = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questionarios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questionarios_Categorias_IdCategoria",
                        column: x => x.IdCategoria,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Questionarios_Usuarios_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Alternativas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Alternativa = table.Column<string>(maxLength: 400, nullable: false),
                    Correta = table.Column<bool>(nullable: false),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    IdPergunta = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alternativas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Alternativas_Perguntas_IdPergunta",
                        column: x => x.IdPergunta,
                        principalTable: "Perguntas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PerguntasConfiguracoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Campo = table.Column<string>(maxLength: 50, nullable: false),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    IdPergunta = table.Column<int>(nullable: false),
                    TipoValor = table.Column<string>(maxLength: 50, nullable: false),
                    Valor = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerguntasConfiguracoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PerguntasConfiguracoes_Perguntas_IdPergunta",
                        column: x => x.IdPergunta,
                        principalTable: "Perguntas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuestionariosPerguntas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ativo = table.Column<bool>(nullable: false),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    IdPergunta = table.Column<int>(nullable: false),
                    IdQuestionario = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionariosPerguntas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionariosPerguntas_Perguntas_IdPergunta",
                        column: x => x.IdPergunta,
                        principalTable: "Perguntas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuestionariosPerguntas_Questionarios_IdQuestionario",
                        column: x => x.IdQuestionario,
                        principalTable: "Questionarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Respostas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Deletado = table.Column<bool>(nullable: false),
                    IdQuestionarioPergunta = table.Column<int>(nullable: false),
                    Resposta = table.Column<string>(maxLength: 1000, nullable: false),
                    Sessao = table.Column<string>(maxLength: 100, nullable: true),
                    TipoValor = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Respostas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Respostas_QuestionariosPerguntas_IdQuestionarioPergunta",
                        column: x => x.IdQuestionarioPergunta,
                        principalTable: "QuestionariosPerguntas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alternativas_IdPergunta",
                table: "Alternativas",
                column: "IdPergunta");

            migrationBuilder.CreateIndex(
                name: "IX_Perguntas_IdCategoria",
                table: "Perguntas",
                column: "IdCategoria");

            migrationBuilder.CreateIndex(
                name: "IX_Perguntas_IdTipoPergunta",
                table: "Perguntas",
                column: "IdTipoPergunta");

            migrationBuilder.CreateIndex(
                name: "IX_Perguntas_IdUsuario",
                table: "Perguntas",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_PerguntasConfiguracoes_IdPergunta",
                table: "PerguntasConfiguracoes",
                column: "IdPergunta");

            migrationBuilder.CreateIndex(
                name: "IX_Questionarios_IdCategoria",
                table: "Questionarios",
                column: "IdCategoria");

            migrationBuilder.CreateIndex(
                name: "IX_Questionarios_IdUsuario",
                table: "Questionarios",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionariosPerguntas_IdPergunta",
                table: "QuestionariosPerguntas",
                column: "IdPergunta");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionariosPerguntas_IdQuestionario",
                table: "QuestionariosPerguntas",
                column: "IdQuestionario");

            migrationBuilder.CreateIndex(
                name: "IX_Respostas_IdQuestionarioPergunta",
                table: "Respostas",
                column: "IdQuestionarioPergunta");

            migrationBuilder.CreateIndex(
                name: "IX_Usuarios_IdPermissao",
                table: "Usuarios",
                column: "IdPermissao");

            migrationBuilder.CreateIndex(
                name: "Usuario",
                table: "Usuarios",
                column: "Usuario",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alternativas");

            migrationBuilder.DropTable(
                name: "PerguntasConfiguracoes");

            migrationBuilder.DropTable(
                name: "Respostas");

            migrationBuilder.DropTable(
                name: "QuestionariosPerguntas");

            migrationBuilder.DropTable(
                name: "Perguntas");

            migrationBuilder.DropTable(
                name: "Questionarios");

            migrationBuilder.DropTable(
                name: "TiposPerguntas");

            migrationBuilder.DropTable(
                name: "Categorias");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.DropTable(
                name: "Permissoes");
        }
    }
}
