using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain.Entities
{
    /// <summary>
    /// Classe PerguntaDomain - Herda características de BaseDomain
    /// </summary>
    public class PerguntaDomain:BaseDomain
    {
        [Required]
        [StringLength(1000, MinimumLength = 4)]
        public String Pergunta { get; set; }

        [Required]
        public Boolean Ativo{ get; set; } = true;

        [Required]
        public Boolean Opcional { get; set; } = false;

        [ForeignKey("IdCategoria")]
        public CategoriaDomain Categoria { get; set; }
        public int IdCategoria { get; set; }

        [ForeignKey("IdTipoPergunta")]
        public TipoPerguntaDomain TipoPergunta { get; set; }
        public int IdTipoPergunta { get; set; }

        [ForeignKey("IdUsuario")]
        public UsuarioDomain Usuario { get; set; }
        public int IdUsuario { get; set; }

        public ICollection<QuestionarioPerguntaDomain> QuestionariosPerguntas { get; set; }
        public ICollection<AlternativaDomain> Alternativas { get; set; }
        public ICollection<PerguntaConfiguracaoDomain> PerguntaConfiguracoes { get; set; }
    }
}