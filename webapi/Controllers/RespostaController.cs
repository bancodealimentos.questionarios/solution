using System;
using System.Linq;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using repository.Context;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe RespostaController
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class RespostaController:Controller
    {
        private IBaseRepository<RespostaDomain> _respostaRepository;
        private IBaseRepository<QuestionarioPerguntaDomain> _questionarioPerguntaRepository;
        private QuestionariosContext _context;

        /// <summary>
        /// Construtor da classe RespostaController
        /// </summary>
        /// <param name="respostaRepository">Retorna o contexto da tabela Respostas</param>
        /// <param name="questionarioPerguntaRepository">Retorna o contexto da tablea QuestionariosPerguntas</param>
        public RespostaController(IBaseRepository<RespostaDomain> respostaRepository,
                                IBaseRepository<QuestionarioPerguntaDomain> questionarioPerguntaRepository,
                                QuestionariosContext context){
            _respostaRepository = respostaRepository;
            _questionarioPerguntaRepository = questionarioPerguntaRepository;
            _context = context;
        }

        /// <summary>
        /// Cadastro de Resposta
        /// </summary>
        /// <param name="resposta">Objeto Resposta</param>
        /// <returns></returns>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para cadastrar a resposta request:
        /// 
        ///     POST /Resposta
        ///     {
        ///         "resposta" : "resposta do usuário",
        ///         "tipoValor" : "Tipo da resposta",
        ///         "questionarioPergunta" : {
        ///             "idQuestionario" : 1,
        ///             "idPergunta" : 1,
        ///         }
        ///      }    
        /// </remarks>
        /// <response code ="200">Cadastro de questionário efetuado com sucesso!"</response>
        /// <response code ="400">Ocorreu um erro ao tentar cadastrar.</response>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(RespostaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Cadastro([FromBody] RespostaDomain resp){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try{
                //QuestionarioPerguntaDomain questionarioPergunta = _questionarioPerguntaRepository.Listar().FirstOrDefault(x => x.IdQuestionario == resp.QuestionarioPergunta.IdQuestionario && x.IdPergunta == resp.QuestionarioPergunta.IdPergunta);
                var questionarioPergunta = _context.QuestionariosPerguntas.FirstOrDefault(x => x.IdQuestionario == resp.QuestionarioPergunta.IdQuestionario && x.IdPergunta == resp.QuestionarioPergunta.IdPergunta);

                if(questionarioPergunta != null)
                {
                    RespostaDomain resposta = new RespostaDomain(){
                        IdQuestionarioPergunta = questionarioPergunta.Id,
                        Resposta = resp.Resposta,
                        TipoValor = resp.TipoValor,
                    };
                    _respostaRepository.Inserir(resposta);
                    return Ok(Json("Realizado com sucesso"));
                }
                return BadRequest("Não foi possível registrar a resposta");
                
            } catch (Exception ex){
                // throw new Exception(ex.Message);
                return BadRequest(ex.Message);
            } finally {
                _context.Dispose();
            }
        }
    }
}