using System;
using System.Collections.Generic;
using System.Linq;
using domain.Contracts;
using domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    /// <summary>
    /// Classe AlternativaController
    /// </summary>
    public class AlternativaController:Controller
    {
        private IBaseRepository<AlternativaDomain> _alternativaRepository;

        /// <summary>
        /// Construtor da classe AlternativaController
        /// </summary>
        /// <param name="alternativaRepository">Retorna o contexto da tabela Alternativas</param>
        public AlternativaController(IBaseRepository<AlternativaDomain> alternativaRepository){
            _alternativaRepository = alternativaRepository;
        }

        /// <summary>
        /// Cadastro de Alternativa
        /// </summary>
        /// <param name="alternativa">Objeto Alternativa</param>
        /// <returns></returns>
        /// <remarks>
        /// Modelo de dados que deve ser enviado para cadastrar a alternativa request:
        /// 
        ///     POST /Alternativa
        ///     {
        ///         
        ///     }    
        /// </remarks>
        /// <response code ="200">Cadastro de alternativa efetuado com sucesso!"</response>
        /// <response code ="400">Ocorreu um erro ao tentar cadastrar.</response>
        [HttpPost]
        [Authorize("Bearer",Roles="Administrador,Palestrante,Monitor")]
        [ProducesResponseType(typeof(AlternativaDomain), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Cadastro([FromBody] ICollection<AlternativaDomain> alternativas){
            if (!ModelState.IsValid){
                var errorList = (from item in ModelState.Values from error in item.Errors select error.ErrorMessage).ToList();
                return BadRequest(Json(errorList));
            }
            try{
                int count = 0;
                foreach (AlternativaDomain alternativa in alternativas){
                    var retorno = _alternativaRepository.Inserir(alternativa);
                    if(retorno.Id != 1){
                        count++;
                    }
                }

                if(count == alternativas.Count){
                    return Ok("");
                } else {
                    List<AlternativaDomain> lsAlternativas = _alternativaRepository.Listar().Where(x => x.IdPergunta == alternativas.FirstOrDefault().IdPergunta).ToList();
                    foreach (AlternativaDomain alternativa in alternativas){
                        _alternativaRepository.Deletar(alternativa);
                    }
                    return BadRequest("Cadastro não efetuado.");
                }
            } catch (Exception ex){
                throw new Exception(ex.Message);
            }
        }
    }
}